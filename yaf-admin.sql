/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50711
 Source Host           : localhost
 Source Database       : yaf-admin

 Target Server Type    : MySQL
 Target Server Version : 50711
 File Encoding         : utf-8

 Date: 06/01/2016 15:56:17 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `ya_admin`
-- ----------------------------
DROP TABLE IF EXISTS `ya_admin`;
CREATE TABLE `ya_admin` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` smallint(5) NOT NULL DEFAULT '0',
  `username` varchar(20) NOT NULL,
  `nickname` varchar(50) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL,
  `salt` varchar(30) NOT NULL DEFAULT '',
  `loginfailure` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '失败次数',
  `logintime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '登录时间',
  `sessionid` varchar(59) NOT NULL,
  `status` varchar(30) NOT NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`),
  KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='管理员表';

-- ----------------------------
--  Records of `ya_admin`
-- ----------------------------
BEGIN;
INSERT INTO `ya_admin` VALUES ('1', '1', 'admin', '超级管理员', '075eaec83636846f51c152f29b98a2fd', 's4f3', '0', '1464767494', '1a7474ac-4410-4cc2-91be-9ab687f5efcf', 'normal'), ('2', '0', 'editor', '编辑', '438186985e1683909dfa095e160bcf84', '', '0', '0', '', 'normal');
COMMIT;

-- ----------------------------
--  Table structure for `ya_attachment`
-- ----------------------------
DROP TABLE IF EXISTS `ya_attachment`;
CREATE TABLE `ya_attachment` (
  `id` int(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `thread_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '帖子ID',
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '附件名称',
  `type` varchar(30) NOT NULL DEFAULT '',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT 'url',
  `size` bigint(10) unsigned NOT NULL DEFAULT '0' COMMENT '附件大小(KB)',
  `filetype` varchar(30) NOT NULL DEFAULT '' COMMENT '文件类型',
  `sign` varchar(50) NOT NULL DEFAULT '' COMMENT '又拍云的Sign值',
  `createtime` int(10) NOT NULL DEFAULT '0' COMMENT '创建日期',
  `updatetime` int(10) NOT NULL DEFAULT '0' COMMENT '上次修改日期',
  PRIMARY KEY (`id`),
  KEY `user_id` (`thread_id`,`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='附件表';

-- ----------------------------
--  Table structure for `ya_block`
-- ----------------------------
DROP TABLE IF EXISTS `ya_block`;
CREATE TABLE `ya_block` (
  `id` smallint(8) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` varchar(30) NOT NULL DEFAULT '' COMMENT '状态',
  `content` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='区块表';

-- ----------------------------
--  Table structure for `ya_configvalue`
-- ----------------------------
DROP TABLE IF EXISTS `ya_configvalue`;
CREATE TABLE `ya_configvalue` (
  `id` varchar(30) NOT NULL DEFAULT '' COMMENT '配置ID',
  `name` varchar(300) NOT NULL DEFAULT '' COMMENT '配置名称',
  `content` varchar(1500) NOT NULL DEFAULT '' COMMENT '配置内容',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` varchar(30) NOT NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='配置表';

-- ----------------------------
--  Records of `ya_configvalue`
-- ----------------------------
BEGIN;
INSERT INTO `ya_configvalue` VALUES ('c:configdata', '基础配置', '{\"cdnurl\":\"\",\"notifyurl\":\"\",\"bucket\":\"minicar\",\"uploadurl\":\"\",\"formkey\":\"\",\"savekey\":\"\\/{year}\\/{mon}\\/{day}\\/{filemd5}{.suffix}\"}', '0', 'normal');
COMMIT;

-- ----------------------------
--  Table structure for `ya_page`
-- ----------------------------
DROP TABLE IF EXISTS `ya_page`;
CREATE TABLE `ya_page` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `flag` varchar(100) NOT NULL DEFAULT '',
  `weigh` int(10) NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `comments` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(30) NOT NULL DEFAULT '' COMMENT '状态',
  `publishtime` int(10) unsigned NOT NULL DEFAULT '0',
  `updatetime` int(10) unsigned NOT NULL DEFAULT '0',
  `createtime` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='单页表';

-- ----------------------------
--  Table structure for `ya_role`
-- ----------------------------
DROP TABLE IF EXISTS `ya_role`;
CREATE TABLE `ya_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `father_id` varchar(36) NOT NULL DEFAULT '0' COMMENT '权限父ID',
  `name` varchar(30) NOT NULL COMMENT '角色名称',
  `rank` int(10) NOT NULL DEFAULT '0' COMMENT '角色权限等级',
  `permission` text NOT NULL COMMENT '角色权限',
  `createtime` int(10) NOT NULL COMMENT '角色创建时间',
  `modifytime` int(10) NOT NULL COMMENT '角色修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='角色表';

-- ----------------------------
--  Records of `ya_role`
-- ----------------------------
BEGIN;
INSERT INTO `ya_role` VALUES ('1', '0', '技术研发部', '100', 'a:8:{s:5:\"index\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}s:8:\"category\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}s:7:\"country\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}s:7:\"ranking\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}s:4:\"page\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}s:4:\"site\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}s:7:\"special\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}s:3:\"tag\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}}', '1342078550', '1366104855'), ('2', '10788df5-15c1-423e-bf1f-40d76304eff0', '运营中心', '50', 'a:3:{s:9:\"allow_uri\";a:1:{i:0;s:0:\"\";}s:8:\"deny_uri\";a:1:{i:0;s:0:\"\";}s:5:\"admin\";a:22:{i:0;s:7:\"general\";s:7:\"general\";a:13:{i:0;s:4:\"main\";i:1;s:6:\"config\";s:6:\"config\";a:3:{i:0;s:5:\"index\";i:1;s:4:\"edit\";i:2;s:7:\"restore\";}i:2;s:7:\"crontab\";s:7:\"crontab\";a:3:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";}i:3;s:8:\"personal\";i:4;s:8:\"denydata\";i:5;s:7:\"version\";s:7:\"version\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}i:6;s:6:\"search\";i:7;s:6:\"notice\";s:6:\"notice\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}i:8;s:5:\"cache\";}i:1;s:4:\"area\";s:4:\"area\";a:6:{i:0;s:4:\"area\";s:4:\"area\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}i:1;s:8:\"realtime\";s:8:\"realtime\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}i:2;s:10:\"statistics\";s:10:\"statistics\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}}i:2;s:7:\"partner\";s:7:\"partner\";a:6:{i:0;s:7:\"partner\";s:7:\"partner\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}i:1;s:8:\"realtime\";s:8:\"realtime\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}i:2;s:10:\"statistics\";s:10:\"statistics\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}}i:3;s:5:\"order\";s:5:\"order\";a:8:{i:0;s:5:\"order\";s:5:\"order\";a:3:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";}i:1;s:8:\"recharge\";s:8:\"recharge\";a:3:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";}i:2;s:10:\"statistics\";s:10:\"statistics\";a:3:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";}i:3;s:4:\"logs\";s:4:\"logs\";a:3:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";}}i:4;s:7:\"product\";s:7:\"product\";a:2:{i:0;s:7:\"product\";s:7:\"product\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}}i:5;s:7:\"account\";s:7:\"account\";a:6:{i:0;s:7:\"account\";s:7:\"account\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}i:1;s:4:\"user\";s:4:\"user\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}i:2;s:5:\"login\";s:5:\"login\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}}i:6;s:10:\"invitecard\";s:10:\"invitecard\";a:6:{i:0;s:10:\"invitecard\";s:10:\"invitecard\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}i:1;s:8:\"activity\";s:8:\"activity\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}i:2;s:3:\"log\";s:3:\"log\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}}i:7;s:8:\"betacode\";s:8:\"betacode\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}i:8;s:8:\"feedback\";s:8:\"feedback\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}i:9;s:4:\"mail\";s:4:\"mail\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}i:10;s:4:\"role\";s:4:\"role\";a:4:{i:0;s:4:\"role\";s:4:\"role\";a:3:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";}i:1;s:5:\"admin\";s:5:\"admin\";a:3:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";}}}}', '1410949031', '1419317757'), ('3', '10788df5-15c1-423e-bf1f-40d76304eff0', '技术专用', '50', 'a:3:{s:9:\"allow_uri\";a:1:{i:0;s:0:\"\";}s:8:\"deny_uri\";a:1:{i:0;s:0:\"\";}s:5:\"admin\";a:6:{i:0;s:7:\"general\";s:7:\"general\";a:4:{i:0;s:4:\"main\";i:1;s:4:\"logs\";i:2;s:8:\"personal\";i:3;s:6:\"search\";}i:1;s:4:\"user\";s:4:\"user\";a:2:{i:0;s:4:\"user\";s:4:\"user\";a:1:{i:0;s:5:\"index\";}}i:2;s:3:\"log\";s:3:\"log\";a:2:{i:0;s:5:\"error\";s:5:\"error\";a:4:{i:0;s:5:\"index\";i:1;s:3:\"add\";i:2;s:4:\"edit\";i:3;s:3:\"del\";}}}}', '1410949569', '1410949620');
COMMIT;

-- ----------------------------
--  Table structure for `ya_version`
-- ----------------------------
DROP TABLE IF EXISTS `ya_version`;
CREATE TABLE `ya_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `oldversion` varchar(30) NOT NULL COMMENT '旧版本号',
  `newversion` varchar(30) NOT NULL DEFAULT '' COMMENT '新版本号',
  `packagesize` varchar(30) NOT NULL DEFAULT '' COMMENT '包大小',
  `content` varchar(500) NOT NULL COMMENT '升级内容',
  `downloadurl` varchar(255) NOT NULL DEFAULT '' COMMENT '下载地址',
  `enforce` tinyint(1) NOT NULL DEFAULT '0' COMMENT '强制更新',
  `createtime` int(10) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `weigh` int(10) NOT NULL DEFAULT '0' COMMENT '权重',
  `status` varchar(30) NOT NULL DEFAULT '' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='应用升级表';

SET FOREIGN_KEY_CHECKS = 1;

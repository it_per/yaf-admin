#Yaf-admin
Yaf后台管理系统

##配置方法
* 将虚拟主机绑定到public目录
* 在system/data目录下创建cache和log目录,并赋予data目录777权限
* 导入yaf-admin.sql
* 如果需要启用二级域名,请修改`system/conf/application.ini`中的`yaf.subdomain`置为1,且将`yaf.modules`的值,改成对应二级域名的前缀

##编译YAF
```
phpize
./configure
$make && make install
```

##YAF文档
http://www.php.net/manual/en/book.yaf.php

##伪静态规则

###Apache
```
#.htaccess
RewriteEngine On
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule .* index.php
```

###Nginx
```
location / {
    root   /var/www/yoursitepath;
    index  index.php index.html index.htm;
    if (!-e $request_filename){
        rewrite ^/(.*)$ /index.php/$1 last;
    }
}
请注意在Nginx下需要增加额外的配置,避免相关隐私信息暴露
location ~ /(application|conf|data|system) {
    deny all;
    return 403;
}
```

##问题反馈
> 在使用中有任何问题，欢迎反馈给我，可以用以下联系方式跟我交流
* Email: (karsonzhang#163.com, 把#换成@)
* weibo: [@karsonzhang](http://weibo.com/karsonzhang)
* Github: https://github.com/karsonzhang
* Git@OSC: https://git.oschina.com/karson

##感激
感谢以下的项目,排名不分先后

* [YAF](https://github.com/laruence/yaf) 





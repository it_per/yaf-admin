<?php

/**
 * 错误控制器
 * 当有未捕获的异常, 则控制流会流到这里
 * @internal
 */
class ErrorController extends Controller {

    public function init() {
        parent::init();
        //关闭视图输出
        Yaf_Dispatcher::getInstance()->disableView();
    }

    /**
     * 错误处理方法
     * @param object $exception
     */
    public function errorAction($exception) {
        switch ($exception->getCode()) {
            case YAF_ERR_NOTFOUND_MODULE:
            case YAF_ERR_NOTFOUND_CONTROLLER:
            case YAF_ERR_NOTFOUND_ACTION:
            case YAF_ERR_NOTFOUND_VIEW:
                $code = 404;
                $content = '404 Not Found';
                break;
            default :
                $code = 500;
                $content = '500 Internal Server Error';
                break;
        }
        if (!$this->_request->isCli()) {
            $this->_response->setHeader($this->_request->getServer('SERVER_PROTOCOL'), $content);
        }
        $this->_response->response();
        Logger::getInstance()->error_handler($exception->getCode(), $exception->getMessage(), $exception->getFile(), $exception->getLine());
        return FALSE;
    }

    public function testAction() {
        
    }

}

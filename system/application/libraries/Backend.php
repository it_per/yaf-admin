<?php

/**
 * 控制器基类
 * 
 * 该类继承自Yaf_Controller_Abstract,可以使用读取或使用Yaf_Controller_Abstract的属性和方法
 */
class Backend extends Yaf_Controller_Abstract {

    /**
     * 返回码,默认为null,当设置了该值后将输出json数据
     * @var int 
     */
    protected $code = null;

    /**
     * 返回内容,默认为null,当设置了该值后将输出json数据
     * @var mixed 
     */
    protected $content = null;

    /**
     * 属性读取魔术方法
     * 
     * 当读取一个不存在的属性时,如果该属性在注册空间中有存在则读取注册空间的值
     * @param string $name 属性值
     * @return \$name
     */
    public function __get($name) {
        if ($name == 'view')
            return $this->_view;
        if (Yaf_Registry::has($name)) {
            return Yaf_Registry::get($name);
        }
    }

    /**
     * 初始化
     */
    public function init() {
        $this->yafAutoRender = TRUE;
        $this->load->library("auth");
        if ($this->_request->getModuleName() == "Index") {
            $this->_request->setModuleName("backend");
            $this->_request->setControllerName(Tools::segment(1, 'index'));
            $this->_request->setActionName(Tools::segment(2, 'index'));
        }
        $this->_view->assign("site", $this->config->site->toArray());
        $modulename = $this->_request->getModuleName();
        $controllername = strtolower($this->_request->getControllerName());
        $actionname = strtolower($this->_request->getActionName());

        if ($actionname != 'login') {
            if ($this->auth->is_login()) {
                if ($this->auth->is_login()) {
                    if (!$this->auth->is_allow(($controllername == 'ajax' ? 'index' : $controllername), $controllername == 'ajax' ? 'index' : $actionname)) {
                        Tools::info("你无权限查看该页面", Tools::siteurl("index/login"));
                    }
                } else {
                    Tools::info("你未登录，请登录后操作", Tools::siteurl("index/login"));
                }
            } else {
                Tools::info("你未登录，请登录后操作", Tools::siteurl("index/login"));
            }
        }
        $this->_view->assign("moduleurl", Tools::siteurl('/'));
        $this->_view->assign("controllername", strtolower($controllername));
        $this->_view->assign("actionname", strtolower($actionname));
        $this->_view->assign("config", $this->config->toArray());
        $this->load->model('configvalue');
        $options = array();
        $configdata = $this->configvalue->values("c:configdata");
        if ($configdata) {
            $options = array(
                'bucket' => $configdata['bucket'],
                'save-key' => $configdata['savekey'],
                'expiration' => time() + 86400
            );
            $policy = base64_encode(json_encode($options));
            $signature = md5($policy . '&' . $configdata['formkey']);
            $options['policy'] = $policy;
            $options['signature'] = $signature;
            $options = array_merge($configdata, $options);
        }
        $this->_view->assign("options", $options);
    }

    /**
     * 首页重定向
     * @return boolean
     */
    public function indexAction() {
        $this->forward("Backend", "Index", "index");
        return FALSE;
    }

    /**
     * 析构方法
     * 
     */
    public function __destruct() {
        
    }

}

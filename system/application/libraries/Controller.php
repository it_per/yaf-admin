<?php

/**
 * 控制器基类
 * 
 * 该类继承自Yaf_Controller_Abstract,可以使用读取或使用Yaf_Controller_Abstract的属性和方法
 */
class Controller extends Yaf_Controller_Abstract {

    /**
     * 返回码,默认为null,当设置了该值后将输出json数据
     * @var int 
     */
    protected $code = null;

    /**
     * 返回内容,默认为null,当设置了该值后将输出json数据
     * @var mixed 
     */
    protected $content = null;

    /**
     * 属性读取魔术方法
     * 
     * 当读取一个不存在的属性时,如果该属性在注册空间中有存在则读取注册空间的值
     * @param string $name 属性值
     * @return \$name
     */
    public function __get($name) {
        if ($name == 'view')
            return $this->_view;
        if (Yaf_Registry::has($name)) {
            return Yaf_Registry::get($name);
        }
    }

    /**
     * 初始化
     */
    public function init() {
        $this->yafAutoRender = TRUE;
        $this->_view->assign("site", $this->config->site->toArray());
    }

    /**
     * 析构方法
     * 
     */
    public function __destruct() {
        
    }

}

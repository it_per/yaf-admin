<?php

class Cache_Xcache extends Cache {

    public static function checkDriver() {
        return extension_loaded('xcache') && function_exists("xcache_get");
    }

    public function __construct() {
        if (!self::checkDriver()) {
            throw new Exception("Can't use this driver for your system!");
        }
    }

    protected function _set($key, $value = "", $time = 300) {
        return xcache_set($key, $value, $time);
    }

    protected function _get($key) {
        $data = xcache_get($key);
        if ($data === false || $data == "") {
            return null;
        }
        return $data;
    }

    protected function _delete($key) {
        return xcache_unset($key);
    }

    protected function _exist($key) {
        if (xcache_isset($key)) {
            return true;
        } else {
            return false;
        }
    }

    protected function _clean() {
        $cnt = xcache_count(XC_TYPE_VAR);
        for ($i = 0; $i < $cnt; $i++) {
            xcache_clear_cache(XC_TYPE_VAR, $i);
        }
        return true;
    }

    protected function _stats() {
        $res = array(
            "info" => "",
            "size" => "",
            "data" => "",
        );

        try {
            $res['data'] = xcache_list(XC_TYPE_VAR, 100);
        } catch (Exception $e) {
            $res['data'] = array();
        }
        return $res;
    }

}

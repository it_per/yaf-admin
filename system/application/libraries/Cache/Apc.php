<?php

class Cache_Apc extends Cache {

    public static function checkDriver() {
        if (extension_loaded('apc') && ini_get('apc.enabled')) {
            return true;
        } else {
            return false;
        }
    }

    public function __construct() {
        if (!self::checkDriver()) {
            throw new Exception("Can't use this driver for your system!");
        }
    }

    protected function _set($key, $value = "", $tts = 300) {
        return apc_store($key, $value, $tts);
    }

    protected function _get($key) {
        $data = apc_fetch($key, $bo);
        if ($bo === false) {
            return null;
        }
        return $data;
    }

    protected function _delete($key) {
        return apc_delete($key);
    }

    protected function _exist($key) {
        if (apc_exists($key)) {
            return true;
        } else {
            return false;
        }
    }

    protected function _clean() {
        @apc_clear_cache();
        @apc_clear_cache("user");
    }

    protected function _stats() {
        $res = array(
            "info" => "",
            "size" => "",
            "data" => "",
        );

        try {
            $res['data'] = apc_cache_info("user");
        } catch (Exception $e) {
            $res['data'] = array();
        }

        return $res;
    }

}

<?php

/**
 * 加载类
 * 
 * 用于加载配置、模型、类或数据
 */
class Load {

    /**
     * 单例模式属性
     * @var Load 
     */
    protected static $_instance = null;

    /**
     * 构造函数
     */
    function __construct() {
        
    }

    /**
     * 单例方法
     * @return Load
     */
    public static function getInstance() {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * 加载一个配置项
     * @param string $name 配置名,配置项名称,可通用$this->config[$name]来读取配置
     * @param string $section 初始化节点名称,默认为null,即读取所有节点
     * @param string $filename 配置文件路径,默认为null,将会到conf中寻找$name.ini文件
     * @param string $overwrite 是否覆盖已有配置,默认为false,当为false时直接返回已有配置,反之覆盖已有配置
     * @return boolean
     */
    public function config($name, $section = null, $filename = null, $overwrite = FALSE) {
        $name = strtolower($name);
        if (isset(Yaf_Registry::get('config')->$name) && !$overwrite)
            return TRUE;
        if (!$filename)
            $filename = BASE_PATH . '/conf/' . $name . '.ini';
        Yaf_Registry::get('config')->$name = new Yaf_Config_Ini($filename, $section ? : '');
        return TRUE;
    }

    /**
     * 加载一个模型项
     * @param string $name 模型名
     * @param string $alias 模型别名,默认为null,为null时和$name相同
     * @param string $filename 模型文件路径,默认情况下框架会自动加载
     * @param mixed $args 传递的参数
     * @return boolean
     */
    public function model($name, $alias = null, $filename = null, $args = null) {
        $name = ucfirst($name);
        $alias = strtolower($alias ? : $name);
        if (Yaf_Registry::has($alias))
            return TRUE;
        if ($filename)
            Yaf_Loader::import($filename);
        $name .= 'Model';
        Yaf_Registry::set($alias, new $name($args));
        return TRUE;
    }

    /**
     * 加载一个通用类项,通用类必须实现一个单例模式的静态方法getInstance
     * @param string $name 类名
     * @param string $alias 类别名,默认为null,为null时和$name相同
     * @param string $filename 类文件路径,默认情况下框架会自动加载
     * @param mixed $args 传递的参数
     * @return boolean
     */
    public function library($name, $alias = null, $filename = null, $args = null) {
        $name = ucfirst($name);
        $alias = strtolower($alias ? : $name);
        if (Yaf_Registry::has($alias))
            return TRUE;
        if ($filename)
            Yaf_Loader::import($filename);
        Yaf_Registry::set($alias, $name::getInstance($args));
        return TRUE;
    }

    /**
     * 加载一个数据库
     * @param string $name 数据库操作类名
     * @param string $alias 数据库别名,默认为null,为null时和$name相同,该项请不要使用db,db已被框架默认初始化
     * @param string $filename 类文件路径,默认情况下框架会自动加载
     * @param mixed $args 传递的参数
     * @return boolean
     */
    public function database($name, $alias = null, $filename = null, $args = null) {
        $name = ucfirst($name);
        $alias = strtolower($alias ? : $name);
        if (Yaf_Registry::has($alias))
            return TRUE;
        if ($filename)
            Yaf_Loader::import($filename);
        Yaf_Registry::set($alias, $name::getInstance($alias, $args));
        return TRUE;
    }

}

<?php

/**
 * 控制器动作基类
 * 
 * 该类继承自Yaf_Action_Abstract,可以使用读取或使用Yaf_Action_Abstract的属性和方法
 */
class Action extends Yaf_Action_Abstract {

    /**
     * 返回码,默认为null,当设置了该值后将输出json数据
     * @var int 
     */
    protected $code = null;

    /**
     * 返回内容,默认为null,当设置了该值后将输出json数据
     * @var mixed 
     */
    protected $content = null;

    /**
     * 属性读取魔术方法
     * 
     * 当读取一个不存在的属性时,如果该属性在注册空间中有存在则读取注册空间的值
     * @param string $name 属性值
     * @return \$name
     */
    public function __get($name) {
        if ($name == 'view')
            return $this->_view;
        if (Yaf_Registry::has($name)) {
            return Yaf_Registry::get($name);
        }
    }

    /**
     * 执行方法
     * 
     * 因为Yaf_Action_Abstract是一个抽象类,所以继承者必须有此方法
     */
    public function execute() {
        
    }

    /**
     * 析构方法
     * 
     * 主要用于设置返回码和返回内容
     */
    public function __destruct() {

    }

}

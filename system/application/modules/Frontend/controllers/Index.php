<?php

/**
 * Frontend
 */
class IndexController extends Controller {

    /**
     * 初始化方法,最前且始终执行
     */
    public function init() {
        parent::init();
    }

    public function indexAction() {
        Tools::info("欢迎使用Yaf-admin<br /><span style='font-weight:400'>当前是首页,<a href='./backend' style='color:#f40'>点击这里访问后台</a></span>", "javascript:;", 0);
        
        // 如果不希望渲染模板,这里需要返回FALSE
        return FALSE;
    }

}

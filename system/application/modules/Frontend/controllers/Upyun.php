<?php

/**
 * 又拍云异步接口
 */
class UpyunController extends Controller {

    public function init() {
        Yaf_Dispatcher::getInstance()->disableView();
    }

    public function notifyAction() {
        $url = $this->_request->getRequest("url");
        $sign = $this->_request->getRequest("sign");
        $user_id = $this->_request->getRequest("ext-param");
        $filetype = $this->_request->getRequest("image-type");
        Tools::d(http_build_query($_POST));
        $user_id = intval($user_id);
        if ($url && $sign) {
            $time = time();
            $params = array(
                'user_id' => $user_id,
                'sign' => $sign,
                'url' => $url,
                'type' => 'image',
                'filetype' => '',
                'createtime' => $time
            );
            $this->db->insert("{pre}attachment", $params);
            echo "success";
        } else {
            echo "failure";
        }
    }

}

<?php

class CategoryController extends Backend {

    private $parentarr = array();

    public function init() {
        parent::init();
        $this->_view->assign(array('title' => '分类', 'nickname' => 'Category', 'intro' => '添加分类、编辑、删除等操作'));
        $this->load->model("form");
        $allcategory = $this->category->all();
        foreach ($allcategory as $k => $v) {
            if ($v['type'] == 2 && $v['parent_id'] == 0 && $v['id']) {
                $this->parentarr[$v['id']] = $v['name'];
            }
        }
        $this->_view->assign("parentlist", $this->parentarr);
    }

    public function indexAction() {
        if ($this->_request->getQuery("draw")) {
            $total = 10000;
            $columns = $this->_request->getQuery("columns");
            $order = $this->_request->getQuery("order");
            $search = $this->_request->getQuery("search");
            $wherearr = array('1=1');
            $orderarr = array('id,DESC');
            $params = array();
            foreach ($columns as $k => $v) {
                if ($v['search'] && $v['search']['value']) {
                    $wherearr[] = "FIND_IN_SET({$v['data']}, ?)";
                    $params[] = $v['search']['value'];
                }
            }
            if ($search && $search['value']) {
                $wherearr[] = "(name LIKE ? OR diyname LIKE ?)";
                $params[] = "%{$search['value']}%";
                $params[] = "%{$search['value']}%";
            }
            if ($order) {
                $orderarr = array();
                foreach ($order as $k => $v) {
                    $orderarr[] = "{$columns[$v['column']]['data']} {$v['dir']}";
                }
            }
            $where = implode(' AND ', $wherearr);
            $orderby = implode(',', $orderarr);
            $draw = $this->_request->getQuery("draw");
            $start = $this->_request->getQuery("start");
            $length = $this->_request->getQuery("length");
            $data = array();
            $this->db->query("SELECT * FROM {pre}category WHERE {$where} ORDER BY {$orderby} LIMIT {$start}, {$length}", $params);
            while ($row = $this->db->fetch()) {
                $row['DT_RowId'] = $row['id'];
                $parentinfo = $this->category->get($row['parent_id']);
                $row['parent_id'] = $parentinfo ? $parentinfo['name'] : "未知[{$row['parent_id']}]";
                $flagarr = array();
                foreach (explode(',', $row['flag']) as $k => $v) {
                    $flagarr[] = isset($this->form->flagarr[$v]) ? $this->form->flagarr[$v] : "未知[{$v}]";
                }
                $row['flag'] = implode(',', $flagarr);
                $row['status'] = isset($this->form->statusarr[$row['status']]) ? $this->form->statusarr[$row['status']] : "未知[{$row['status']}]";
                $data[] = $row;
            }
            $result = array("draw" => $draw, "recordsTotal" => $total, "recordsFiltered" => $total, "data" => $data);

            echo json_encode($result);
            return FALSE;
        }
        $this->_view->assign("flaglist", $this->form->flagarr);
        $this->_view->assign("statuslist", $this->form->statusarr);
    }

    public function addAction() {
        if ($this->_request->getMethod() == "POST") {
            $params = $this->_request->getPost("row");
            foreach ($params as $k => & $v) {
                $v = is_array($v) ? Tools::encode($v) : $v;
            }
            if ($params) {
                $this->db->insert("{pre}category", $params);
            }
            echo json_encode(array('code' => 0, 'content' => ''));
            exit;
        }
        $this->_view->assign("parentoptions", $this->form->options($this->parentarr, 0));
        $this->_view->assign("flagoptions", $this->form->flagoptions());
    }

    public function editAction($ids = NULL) {
        if ($this->_request->getMethod() == "POST") {
            $params = $this->_request->getPost("row");
            foreach ($params as $k => & $v) {
                $v = is_array($v) ? Tools::encode($v) : $v;
            }
            if ($params) {
                $this->db->update("{pre}category", $params, array('id' => $ids));
            }
            echo json_encode(array('code' => 0, 'content' => ''));
            exit;
        }
        $row = $this->db->one("SELECT * FROM {pre}category WHERE id = ?", array($ids));
        $this->_view->assign("row", $row);
        $this->_view->assign("parentoptions", $this->form->options($this->parentarr, $row['parent_id']));
        $this->_view->assign("flagoptions", $this->form->flagoptions($row['flag']));
    }

    public function delAction($ids = "") {
        $code = -1;
        $content = '';
        if ($ids) {
            $this->db->delete("{pre}category", "id IN ({$ids})");
            $count = $this->db->count();
            if ($count) {
                $code = 0;
                $content = "操作成功！共删除{$count}条数据！";
            } else {
                $content = "操作失败！共删除0条数据！";
            }
        }
        echo json_encode(array('code' => $code, 'content' => $content));
        exit;
    }

    public function multiAction($ids = "") {
        $code = -1;
        $content = '';
        if ($ids) {
            parse_str($this->_request->getPost("params"), $values);
            if ($values) {
                $this->db->update("{pre}category", $values, "id IN ({$ids})");
                $count = $this->db->count();
                if ($count) {
                    $code = 0;
                    $content = "操作成功！共更新{$count}条数据！";
                } else {
                    $content = "操作失败！共更新0条数据！";
                }
            } else {
                $content = "操作失败！共更新0条数据！";
            }
        }
        echo json_encode(array('code' => $code, 'content' => $content));
        exit;
    }

}

<?php

class CommonPlugin extends Yaf_Plugin_Abstract {

    public function __construct() {
        
    }

    //路由初始化前
    public function routerStartup(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
        
    }

    public function routerShutdown(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
        $modulename = ucfirst($request->getModuleName());
        /**
         * 定义当前模块路径
         */
        !defined('MODULE_PATH') && define('MODULE_PATH', $modulename == 'Index' || $modulename == 'Frontend' ? APPLICATION_PATH : APPLICATION_PATH . '/modules/' . $modulename);

        /**
         * 定义视图目录路径
         */
        !defined('VIEW_PATH') && define('VIEW_PATH', MODULE_PATH . '/views');
    }

    public function dispatchLoopStartup(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
        
    }

    public function preDispatch(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
        
    }

    public function postDispatch(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
        
    }

    public function dispatchLoopShutdown(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
        
    }

    public function preResponse(Yaf_Request_Abstract $request, Yaf_Response_Abstract $response) {
        
    }

}

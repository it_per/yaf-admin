<?php

/**
 * Bootstrap启动器
 * 用于加载一些能用配置,所有方法名均以_init开头,并依次进行调用
 */
class Bootstrap extends Yaf_Bootstrap_Abstract {

    /**
     * 错误捕获
     * @param Yaf_Dispatcher $dispatcher
     */
    public function _initErrorHandler(Yaf_Dispatcher $dispatcher) {
        register_shutdown_function(array(Logger::getInstance(), 'shutdown_handler'));
        $dispatcher->setErrorHandler(array(Logger::getInstance(), 'error_handler'));
    }

    /**
     * 加载配置
     * @param Yaf_Dispatcher $dispatcher
     */
    public function _initConfig(Yaf_Dispatcher $dispatcher) {
        define('DATA_PATH', BASE_PATH . '/data');
        
        //加载基础配置
        Yaf_Registry::set("config", Yaf_Application::app()->getConfig());
        //加载游戏配置
        //Load::getInstance()->config("game", "game");
    }

    /**
     * SESSION初始化
     * @param Yaf_Dispatcher $dispatcher
     */
    public function _initSession(Yaf_Dispatcher $dispatcher) {
        Yaf_Session::getInstance()->start();
    }

    /**
     * Cookie初始化
     * @param Yaf_Dispatcher $dispatcher
     */
    public function _initCookie(Yaf_Dispatcher $dispatcher) {
        Load::getInstance()->library("cookie", "cookie");
    }

    /**
     * 加载加载器
     * @param Yaf_Dispatcher $dispatcher
     */
    public function _initLoad(Yaf_Dispatcher $dispatcher) {
        Yaf_Loader::getInstance()->import(APPLICATION_PATH . '/helpers/Common.php');
        Yaf_Registry::set("load", Load::getInstance());
    }

    /**
     * 加载缓存
     * @param Yaf_Dispatcher $dispatcher
     */
    public function _initCache(Yaf_Dispatcher $dispatcher) {
        Yaf_Registry::set("cache", Cache::getInstance());
    }

    /**
     * 数据库初始化
     * @param Yaf_Dispatcher $dispatcher
     */
    public function _initDatabase(Yaf_Dispatcher $dispatcher) {
        Load::getInstance()->database('Database', 'db', null, Yaf_Registry::get("config")->database->mysql->toArray());
    }

    /**
     * 加载插件
     * @param Yaf_Dispatcher $dispatcher
     */
    public function _initPlugin(Yaf_Dispatcher $dispatcher) {
        $dispatcher->registerPlugin(new CommonPlugin());
    }

    /**
     * 加载路由配置
     * @param Yaf_Dispatcher $dispatcher
     */
    public function _initRoute(Yaf_Dispatcher $dispatcher) {
        $router = Yaf_Dispatcher::getInstance()->getRouter();
        //从ini配置中读取自定义路由协议
        if (Yaf_Registry::get("config")->routes) {
            $router->addConfig(Yaf_Registry::get("config")->routes);
        }
        //添加二级域名路由协议
        $router->addRoute('domainroute', new Routeplus());
    }

}

<?php

/**
 * 表单类
 */
class FormModel extends Model {

    public $flagarr = array('t' => '头条[t]', 'i' => '首页[i]', 'r' => '推荐[r]', 'h' => '热门[h]', 'p' => '图片[p]', 'j' => '跳转[j]');
    public $statusarr = array('normal' => '<font color=green>正常</font>', 'hidden' => '<font color=gray>隐藏</font>', 'prepare' => '<font color=blue>准备</font>');

    function __construct() {
        parent::__construct();
    }

    function select($name = '', $options = array(), $selected = array(), $attr = array()) {
        $extra = parse_attr($attr);
        if (!is_array($selected)) {
            $selected = array($selected);
        }
        if ($extra != '')
            $extra = ' ' . $extra;
        $multiple = (count($selected) > 1 && strpos($extra, 'multiple') === FALSE) ? ' multiple="multiple"' : '';
        $form = '<select name="' . $name . '"' . $extra . $multiple . ">\n";
        $form .= $this->option($options, $selected);
        $form .= '</select>';
        return $form;
    }

    function options($options = array(), $selected = array()) {
        if (!is_array($selected)) {
            $selected = explode(',', $selected);
        }
        $form = '';
        foreach ($options as $key => $val) {
            $key = (string) $key;
            if (is_array($val)) {
                $form .= '<optgroup label="' . $key . '">' . "\n";
                foreach ($val as $optgroup_key => $optgroup_val) {
                    $sel = (in_array($optgroup_key, $selected)) ? ' selected="selected"' : '';
                    $form .= '<option value="' . $optgroup_key . '"' . $sel . '>' . $optgroup_val . "</option>\n";
                }
                $form .= '</optgroup>' . "\n";
            } else {
                $sel = (in_array($key, $selected)) ? ' selected="selected"' : '';
                $form .= '<option value="' . $key . '"' . $sel . '>' . $val . "</option>\n";
            }
        }
        return $form;
    }

    function categoryoptions($type = 2, $selected = array()) {
        $selected = is_array($selected) ? $selected : explode(',', $selected);
        $allcategory = $this->category->all();
        $countrylist = array();
        $categorylist = array();
        $secondcategorylist = array();
        foreach ($allcategory as $k => $v) {
            if ($v['type'] == $type) {
                if ($v['parent_id'] == 0) {
                    $categorylist[$v['id']]['categoryinfo'] = $v;
                    $categorylist[$v['id']]['sublist'] = array();
                } else {
                    //$firstcategorylist[$v['parent_id']]['childlist'][] = $v;
                    //$secondcategorylist[$v['id']] = $v;
                }
            }
        }
        foreach ($allcategory as $k => $v) {
            if ($v['type'] == $type && $v['parent_id'] != 0) {
                $categorylist[$v['parent_id']]['sublist'][] = $v;
            }
        }
        $optionsarr = array();
        foreach ($categorylist as $k => $v) {
            $optionsarr[$v['categoryinfo']['name']] = array();
            foreach ($v['sublist'] as $m => $n) {
                $optionsarr[$v['categoryinfo']['name']][$n['id']] = $n['name'] . "({$n['diyname']})";
            }
        }
        return $this->options($optionsarr, $selected);
    }

    function countryoptions($selected = array()) {
        $selected = is_array($selected) ? $selected : explode(',', $selected);
        $this->load->model("category");
        $this->load->model("country");
        $parentcountrylist = $this->category->multi(array(1, 2, 3, 4, 5));
        $countrylist = $this->country->all();
        $optionsarr = array();
        $optionsarr[0] = "全球";
        $parentoptionarr = array();
        foreach ($countrylist as $m => $n) {
            $parentoptionarr[$n['category_id']][$n['id']] = $n['name'] . "({$n['diyname']})";
        }
        foreach ($parentcountrylist as $k => $v) {
            $optionsarr[$v['name']] = isset($parentoptionarr[$v['id']]) ? $parentoptionarr[$v['id']] : array();
        }
        return $this->options($optionsarr, $selected);
    }

    function specialoptions($selected = array()) {
        $this->load->model("special");
        $speciallist = $this->special->all();
        $optionsarr = array();
        foreach ($speciallist as $k => $v) {
            $optionsarr[$v['id']] = $v['title'];
        }
        return $this->options($optionsarr, $selected);
    }

    function tagoptions($selected = array()) {
        $this->load->model("tag");
        $speciallist = $this->tag->multi($selected);
        $optionsarr = array();
        foreach ($speciallist as $k => $v) {
            $optionsarr[$v['id']] = $v['name'] . "({$v['diyname']})";
        }
        return $this->options($optionsarr, $selected);
    }

    function siteoptions($selected = array()) {
        $this->load->model("site");
        $speciallist = $this->site->multi($selected);
        $optionsarr = array();
        foreach ($speciallist as $k => $v) {
            $optionsarr[$v['id']] = $v['title'];
        }
        return $this->options($optionsarr, $selected);
    }

    function flagoptions($selected = array()) {
        $optionsarr = $this->flagarr;
        return $this->options($optionsarr, $selected);
    }

    function countryarr() {
        $this->load->model("category");
        $this->load->model("country");
        $parentcountrylist = $this->category->multi(array(1, 2, 3, 4, 5));
        $countrylist = $this->country->all();
        $optionsarr = array();
        $optionsarr[0] = "全球";
        $parentoptionarr = array();
        foreach ($countrylist as $m => $n) {
            $parentoptionarr[$n['category_id']][$n['id']] = $n;
        }
        foreach ($parentcountrylist as $k => $v) {
            $optionsarr[$v['id']] = "<b>" . $v['name'] . "</b>";
            if (isset($parentoptionarr[$v['id']])) {
                foreach ($parentoptionarr[$v['id']] as $m => $n) {
                    $optionsarr[$n['id']] = "┣ " . $n['name'] . "({$n['diyname']})";
                }
            }
        }
        return $optionsarr;
    }

    function categoryarr($type = 2) {
        $allcategory = $this->category->all();
        $categorylist = array();
        foreach ($allcategory as $k => $v) {
            if ($v['type'] == $type) {
                if ($v['parent_id'] == 0) {
                    $categorylist[$v['id']]['categoryinfo'] = $v;
                    $categorylist[$v['id']]['sublist'] = array();
                } else {
                    //$firstcategorylist[$v['parent_id']]['childlist'][] = $v;
                    //$secondcategorylist[$v['id']] = $v;
                }
            }
        }
        foreach ($allcategory as $k => $v) {
            if ($v['type'] == $type && $v['parent_id'] != 0) {
                $categorylist[$v['parent_id']]['sublist'][$v['id']] = $v;
            }
        }
        $optionsarr = array();
        foreach ($categorylist as $k => $v) {
            $optionsarr[implode(',', array_keys($v['sublist']))] = "<b>{$v['categoryinfo']['name']}</b>";
            foreach ($v['sublist'] as $m => $n) {
                $optionsarr[$n['id']] = "┣ " . $n['name'] . "({$n['diyname']})";
            }
        }
        return $optionsarr;
    }

}

<?php

/**
 * 分类模块
 * @version $Id: Category.php 567 2015-02-05 11:52:51Z karson $
 */
class CategoryModel Extends Model {

    public $key = "category";

    public function __construct() {
        parent::__construct(array('flag'));
    }

    /**
     * 读取所有分类信息
     */
    public function all() {
        return Tools::getArraySort(array_filter(parent::all()), 'weigh', 'SORT_DESC', 'id', 'SORT_ASC');
    }

}

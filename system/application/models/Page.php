<?php

/**
 * 单页模块
 * @version $Id: Page.php 567 2015-02-05 11:52:51Z karson $
 */
class PageModel Extends Model {

    public $key = "page";
    public $id = 0;
    public $fields = array();

    /**
     * 构造函数
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * 读取所有版本号信息
     */
    public function all() {
        return Tools::getArraySortIndex(parent::all(), 'weigh', 'SORT_DESC', 'id', 'SORT_DESC');
    }
    
}

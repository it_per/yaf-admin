<?php

/**
 * 区块模块
 * @version $Id: Block.php 567 2015-02-05 11:52:51Z karson $
 */
class BlockModel Extends Model {

    public $key = "block";

    /**
     * 构造函数
     */
    public function __construct() {
        parent::__construct();
    }

}
